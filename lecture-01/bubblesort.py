#!/usr/bin/env python3

# Example of sorting an array using bubblesort


arr = [0, 9, 1, 8, 2, 7, 3, 6, 4, 5]
#arr = [9, 0]
#arr = [2]


def bubble_down(arr, arr_len):
    """
    Performs a series of in-place swaps starting at the beginning of the array and ending at arr_len index.

    It assumes that arr_len >= 2.
    """
    for idx in range(arr_len - 1):
        if arr[idx] > arr[idx + 1]:
            swap_var = arr[idx]
            arr[idx] = arr[idx + 1]
            arr[idx + 1] = swap_var
            # alternatively: arr[idx], arr[idx + 1] = arr[idx + 1], arr[idx]


def bubble_sort(arr):
    """
    Sorts an array in-line in an ascending order using bubble sort.
    """
    arr_len = len(arr)

    while arr_len >= 2:
        bubble_down(arr, arr_len)
        arr_len -= 1



print(arr)

bubble_sort(arr)

print(arr)

