#!/usr/bin/env python3

# Example of printing out a multiplication table of a given number

def print_row(row_idx, size):
    for col_idx in range(size):
        value = (col_idx + 1) * (row_idx + 1)
        print(value, end="   ")

def print_multiplication_table(size):
    for row_idx in range(size):
        print_row(row_idx, size)
        print()

print_multiplication_table(10)
