#!/usr/bin/env python3


"""
       *
      * *
     *   *
    *     *
   *       *
  * *     * *
 *   *   *   *
* * * * * * * *

"""

def print_tree_level(size, level):
    if size == 0:
        print("* ")
    else:
        levels = 2 ** size

        for idx in range(levels - level - 1):
            print(" ", end="")

        print("*", end="")

        if level > 0:
            for idx in range(level):
                print(" ", end="")

            print("* ", end="")


def print_tree(size, count):
    levels = 2 ** (size - 1)
    for idx in range(levels):
        for cnt_idx in range(count):
            print_tree_level(size, idx)
        print()

    if size > 0:
        print_tree(size - 1, count * 2)


print_tree(2, 1)