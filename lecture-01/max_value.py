#!/usr/bin/env python3


arr = [0, 9, 1, 8, 2, 7, 3, 6, 4, 5]
max = 0
i = 0

while i < len(arr):
    if arr[i] > max:
        max = arr[i]
    i += 1
print(max)
