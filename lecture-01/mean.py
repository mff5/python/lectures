#!/usr/bin/env python3

# Example of a function that computes mean


arr = [0, 9, 1, 8, 2, 7, 3, 6, 4, 5]

def mean(arr):
    sum = 0

    i = 0
    while i < len(arr):
        sum += arr[i]
        i += 1

    return sum / len(arr)


print(mean(arr))